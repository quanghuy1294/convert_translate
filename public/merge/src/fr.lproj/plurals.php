<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d jour par semaine\", \"other\":\"%d jours par semaine\"}";
$a["streak||count"] = "{\"one\":\"Série réussie\", \"other\":\"%d Séries\"}";
$a["more||count"] = "{\"one\":\"%d de plus\", \"other\":\"%d de plus\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u autres habitudes le %@\", \"other\":\"%u autres habitudes le %@\"}";
$a["interval.day||count"] = "{\"one\":\"Tous les %@ jours\", \"other\":\"Tous les %d jours\"}";
$a["streak.day||count"] = "{\"one\":\"%d jours consécutifs!\", \"other\":\"%d jours consécutifs!\"}";
$a["month||count"] = "{\"one\":\"%d mois\", \"other\":\"%d mois\"}";
$a["passed.week||count"] = "{\"one\":\"La dernière semaine\", \"other\":\"Les %d dernières semaines\"}";
$a["passed.days||count"] = "{\"one\":\"Les %d derniers jours\", \"other\":\"Les %d derniers jours\"}";
$a["times||count"] = "{\"one\":\"%d temps\", \"other\":\"%d fois\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d jours consécutifs\", \"other\":\"%d jours consécutifs\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ de %1d habitudes terminées\", \"other\":\"%2#@completed@ de %1d habitudes terminée\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d jours consécutifs\", \"other\":\"%d jours consécutifs\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d jour consécutif\", \"other\":\"%d jours consécutifs\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d habitude\", \"other\":\"%d habitudes\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d habitude\", \"other\":\"%d habitudes\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d habitude\", \"other\":\"%d habitudes\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d habitude\", \"other\":\"%d habitudes\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d habitude\", \"other\":\"%d habitudes\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d jours consécutifs\", \"other\":\"%d jours consécutifs\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d habitude\", \"other\":\"%d habitudes\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d habitude\", \"other\":\"%d habitudes\"}";

return $a;
?>