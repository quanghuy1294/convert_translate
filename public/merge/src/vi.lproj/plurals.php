<?php
$a = [];
$a["day.per.week||count"] = "{\"other\":\"%d ngày một tuần\"}";
$a["streak||count"] = "{\"other\":\"%d ngày liên tiếp\"}";
$a["more||count"] = "{\"other\":\"Thêm %d\"}";
$a["time.of.day.notification.group||notifications"] = "{\"other\":\"%u thói quen nữa trong buổi %@\"}";
$a["interval.day||count"] = "{\"other\":\"Mỗi %d ngày\"}";
$a["streak.day||count"] = "{\"other\":\"%d ngày liên tiếp!\"}";
$a["month||count"] = "{\"other\":\"%d tháng\"}";
$a["passed.week||count"] = "{\"other\":\"%d tuần trước\"}";
$a["passed.days||count"] = "{\"other\":\"%d ngày trước\"}";
$a["times||count"] = "{\"other\":\"%d lần\"}";
$a["streak.day.journal||count"] = "{\"other\":\"%d Ngày liên tiếp\"}";
$a["habit.remaining||required"] = "{\"other\":\"%2#@completed@ trên %1d thói quen được hoàn thành\"}";
$a["habit.remaining||completed"] = "{\"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"other\":\"%d ngày liên tiếp\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"other\":\"%d ngày liên tiếp\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"other\":\"%d thói quen\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"other\":\"%d thói quen\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"other\":\"%d thói quen\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"other\":\"%d thói quen\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"other\":\"%d thói quen\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"other\":\"%d ngày liên tiếp\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"other\":\"%d thói quen\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"other\":\"%d thói quen\"}";

return $a;
?>