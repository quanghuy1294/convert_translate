<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/original', ['uses' => 'ImportOriginalController@index']);
Route::get('/import', ['uses' => 'ImportOriginalController@importLang', 'as' => 'import.import']);
Route::any('/store', ['uses' => 'ImportOriginalController@store', 'as' => 'import.store']);

Route::get('/importTemplate', ['uses' => 'ImportOriginalController@importTemplate', 'as' => 'import.importTemplate']);
Route::any('/storeTemplate', ['uses' => 'ImportOriginalController@storeTemplate', 'as' => 'import.storeTemplate']);
Route::any('/generator', ['uses' => 'ImportOriginalController@generator', 'as' => 'import.generator']);
Route::any('/generatorPolish', ['uses' => 'ImportOriginalController@generatorPolish', 'as' => 'import.generatorPolish']);

Route::any('/readXliff', ['uses' => 'ImportOriginalController@readXliff', 'as' => 'import.readXliff']);
Route::any('/mergeForm', ['uses' => 'ImportOriginalController@mergeForm', 'as' => 'import.mergeForm']);
Route::any('/mergeProcess', ['uses' => 'ImportOriginalController@mergeProcess', 'as' => 'import.mergeProcess']);

Route::any('/readPlural', ['uses' => 'ImportOriginalController@readPlural', 'as' => 'import.readPlural']);

Route::any('/convert', function () {
    $a = '{"one":"%2d#@completed@ of %1d habit completed", "other":"%2d#@completed@ of %1d habits completed"}';
    dd($a, json_decode($a));
});
