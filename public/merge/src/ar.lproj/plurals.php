<?php
$a = [];
$a["day.per.week||count"] = "{\"zero\":\"%d days per week\", \"one\":\"%d day per week\", \"two\":\"%d days per week\", \"few\":\"%d days per week\", \"many\":\"%d days per week\", \"other\":\"%d days per week\"}";
$a["streak||count"] = "{\"zero\":\"%d Streaks\", \"one\":\"%d Streak\", \"two\":\"%d Streaks\", \"few\":\"%d Streaks\", \"many\":\"%d Streaks\", \"other\":\"%d Streaks\"}";
$a["more||count"] = "{\"zero\":\"%d more\", \"one\":\"%d more\", \"two\":\"%d more\", \"few\":\"%d more\", \"many\":\"%d more\", \"other\":\"%d more\"}";
$a["time.of.day.notification.group||notifications"] = "{\"zero\":\"%u more habits in %@\", \"one\":\"%u more habit in %@\", \"two\":\"%u more habits in %@\", \"few\":\"%u more habits in %@\", \"many\":\"%u more habits in %@\", \"other\":\"%u more habits in %@\"}";
$a["interval.day||count"] = "{\"zero\":\"Every %d days\", \"one\":\"Every %d day\", \"two\":\"Every %d days\", \"few\":\"Every %d days\", \"many\":\"Every %d days\", \"other\":\"Every %d days\"}";
$a["streak.day||count"] = "{\"zero\":\"%d days streaks!\", \"one\":\"%d day streaks!\", \"two\":\"%d days streaks!\", \"few\":\"%d days streaks!\", \"many\":\"%d days streaks!\", \"other\":\"%d days streaks!\"}";
$a["month||count"] = "{\"zero\":\"%d months\", \"one\":\"%d month\", \"two\":\"%d months\", \"few\":\"%d months\", \"many\":\"%d months\", \"other\":\"%d months\"}";
$a["passed.week||count"] = "{\"zero\":\"Last %d weeks\", \"one\":\"Last %d week\", \"two\":\"Last %d weeks\", \"few\":\"Last %d weeks\", \"many\":\"Last %d weeks\", \"other\":\"Last %d weeks\"}";
$a["passed.days||count"] = "{\"zero\":\"Last %d days\", \"one\":\"Last %d day\", \"two\":\"Last %d days\", \"few\":\"Last %d days\", \"many\":\"Last %d days\", \"other\":\"Last %d days\"}";
$a["times||count"] = "{\"zero\":\"%d times\", \"one\":\"%d time\", \"two\":\"%d times\", \"few\":\"%d times\", \"many\":\"%d times\", \"other\":\"%d times\"}";
$a["streak.day.journal||count"] = "{\"zero\":\"%d-day streak\", \"one\":\"%d-day streak\", \"two\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["habit.remaining||required"] = "{\"zero\":\"%2#@completed@ of %1d habits completed\", \"one\":\"%2#@completed@ of %1d habit completed\", \"two\":\"%2#@completed@ of %1d habits completed\", \"few\":\"%2#@completed@ of %1d habits completed\", \"many\":\"%2#@completed@ of %1d habits completed\", \"other\":\"%2#@completed@ of %1d habits completed\"}";
$a["habit.remaining||completed"] = "{\"zero\":\"%d\", \"one\":\"%d\", \"two\":\"%d\", \"few\":\"%d\", \"many\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"zero\":\"%d-day streak\", \"one\":\"%d-day streak\", \"two\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"zero\":\"%d-day streak\", \"one\":\"%d-day streak\", \"two\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"zero\":\"%d habits\", \"one\":\"%d habit\", \"two\":\"%d habits\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"zero\":\"%d habits\", \"one\":\"%d habit\", \"two\":\"%d habits\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"zero\":\"%d habits\", \"one\":\"%d habit\", \"two\":\"%d habits\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"zero\":\"%d habits\", \"one\":\"%d habit\", \"two\":\"%d habits\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"zero\":\"%d habits\", \"one\":\"%d habit\", \"two\":\"%d habits\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"zero\":\"%d-day streak\", \"one\":\"%d-day streak\", \"two\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"zero\":\"%d habits\", \"one\":\"%d habit\", \"two\":\"%d habits\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"zero\":\"%d habits\", \"one\":\"%d habit\", \"two\":\"%d habits\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";

return $a;
?>