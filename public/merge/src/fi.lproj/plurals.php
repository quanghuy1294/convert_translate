<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d päivä viikossa\", \"other\":\"%d päivää viikossa\"}";
$a["streak||count"] = "{\"one\":\"%d putki\", \"other\":\"%d putki\"}";
$a["more||count"] = "{\"one\":\"%d lisää\", \"other\":\"%d lisää\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u tapa lisää %@\", \"other\":\"%u tapaa lisää %@\"}";
$a["interval.day||count"] = "{\"one\":\"Joka päivä\", \"other\":\"Joka %d päivää\"}";
$a["streak.day||count"] = "{\"one\":\"%d päivän putki!\", \"other\":\"%d päivän putki!\"}";
$a["month||count"] = "{\"one\":\"%d kuukausi\", \"other\":\"%d kuukautta\"}";
$a["passed.week||count"] = "{\"one\":\"Viime viikolla\", \"other\":\"Viimeiset %d viikkoa\"}";
$a["passed.days||count"] = "{\"one\":\"Eilen\", \"other\":\"Viimeiset %d päivää\"}";
$a["times||count"] = "{\"one\":\"%d kerta\", \"other\":\"%d kertaa\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d-päivän putki\", \"other\":\"%d-päivän putki\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ / %1d tapaa tehty\", \"other\":\"%2#@completed@ / %1d tapaa tehty\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d-päivän putki\", \"other\":\"%d-päivän putki\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d-päivän putki\", \"other\":\"%d-päivän putki\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d tapa\", \"other\":\"%d tapaa\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d tapa\", \"other\":\"%d tapaa\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d tapa\", \"other\":\"%d tapaa\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d tapa\", \"other\":\"%d tapaa\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d tapa\", \"other\":\"%d tapaa\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d-päivän putki\", \"other\":\"%d-päivän putki\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d tapa\", \"other\":\"%d tapaa\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d tapa\", \"other\":\"%d tapaa\"}";

return $a;
?>