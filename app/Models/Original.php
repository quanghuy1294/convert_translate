<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Original extends Model
{
    protected $table = "original";
    protected $fillable = ['key', 'value'];
}
