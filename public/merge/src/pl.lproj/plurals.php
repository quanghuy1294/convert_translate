<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d dzień w tygodniu\", \"few\":\"%d dni na tydzień\", \"many\":\"%d dni w tygodniu\", \"other\":\"%d dnia na tydzień\"}";
$a["streak||count"] = "{\"one\":\"%d seria\", \"few\":\"%d serie\", \"many\":\"%d serii\", \"other\":\"%d serii\"}";
$a["more||count"] = "{\"one\":\"%d więcej\", \"few\":\"%d więcej\", \"many\":\"%d więcej\", \"other\":\"%d więcej\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u nawyk więcej w %@\", \"few\":\"%u nawyki więcej w %@\", \"many\":\"%u nawyków więcej w %@\", \"other\":\"%u nawyków więcej w %@\"}";
$a["interval.day||count"] = "{\"one\":\"Codziennie\", \"few\":\"Co %d dni\", \"many\":\"Co %d dni\", \"other\":\"Co %d dnia\"}";
$a["streak.day||count"] = "{\"one\":\"%d dzień serii!\", \"few\":\"%d dni serii!\", \"many\":\"%d dni serii!\", \"other\":\"%d dnia serii!\"}";
$a["month||count"] = "{\"one\":\"%d miesiąc\", \"few\":\"%d miesiące\", \"many\":\"%d miesiący\", \"other\":\"%d miesiąca\"}";
$a["passed.week||count"] = "{\"one\":\"Ostatni tydzień\", \"few\":\"Ostatnie %d tygodnie\", \"many\":\"Ostatnie %d tygodni\", \"other\":\"Ostatnie %d tygodnia\"}";
$a["passed.days||count"] = "{\"one\":\"Ostatni dzień\", \"few\":\"Ostatnie %d dni\", \"many\":\"Ostatnie %d dni\", \"other\":\"Ostatnie %d dnia\"}";
$a["times||count"] = "{\"one\":\"%d raz\", \"few\":\"%d razy\", \"many\":\"%d razy\", \"other\":\"%d raza\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d dzień serii\", \"few\":\"%d dni serii\", \"many\":\"%d dni serii\", \"other\":\"%d dnia serii\"}";
$a["habit.remaining||required"] = "{\"one\":\"Ukończono %2#@completed@ z %1d nawyku\", \"few\":\"Ukończono %2#@completed@ z %1d nawyków\", \"many\":\"Ukończono %2#@completed@ z %1d nawyków\", \"other\":\"Ukończono %2#@completed@ z %1d nawyków\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"few\":\"%d\", \"many\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d dniu serii\", \"few\":\"%d dniach serii\", \"many\":\"%d dniach serii\", \"other\":\"%d dnia serii\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d dniowej serii\", \"few\":\"%d dniowej serii\", \"many\":\"%d dniowej serii\", \"other\":\"%d dniowej serii\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d nawyk\", \"few\":\"%d nawyki\", \"many\":\"%d nawyków\", \"other\":\"%d nawyku\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d nawyk\", \"few\":\"%d nawyki\", \"many\":\"%d nawyków\", \"other\":\"%d nawyku\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d nawyk\", \"few\":\"%d nawyki\", \"many\":\"%d nawyków\", \"other\":\"%d nawyku\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d nawyk\", \"few\":\"%d nawyki\", \"many\":\"%d nawyków\", \"other\":\"%d nawyku\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d nawyk\", \"few\":\"%d nawyki\", \"many\":\"%d nawyków\", \"other\":\"%d nawyku\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d dniem serii\", \"few\":\"%d dniami serii\", \"many\":\"%d dniami serii\", \"other\":\"%d dnia serii\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d nawyk\", \"few\":\"%d nawyki\", \"many\":\"%d nawyków\", \"other\":\"%d nawyku\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d nawyk\", \"few\":\"%d nawyki\", \"many\":\"%d nawyków\", \"other\":\"%d nawyku\"}";

return $a;
?>