<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d dag per week\", \"other\":\"%d dagen per week\"}";
$a["streak||count"] = "{\"one\":\"%d overwinningsreeks\", \"other\":\"%d overwinningsreeks\"}";
$a["more||count"] = "{\"one\":\"Nog %d\", \"other\":\"Nog %d\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"Nog %u gewoonte in %@\", \"other\":\"Nog %u gewoonten in %@\"}";
$a["interval.day||count"] = "{\"one\":\"Elke dag\", \"other\":\"Elke %d dagen\"}";
$a["streak.day||count"] = "{\"one\":\"Reeks van %d dag!\", \"other\":\"Reeks van %d dagen!\"}";
$a["month||count"] = "{\"one\":\"%d maand\", \"other\":\"%d maanden\"}";
$a["passed.week||count"] = "{\"one\":\"Vorige week\", \"other\":\"Vorige %d weken\"}";
$a["passed.days||count"] = "{\"one\":\"Gisteren\", \"other\":\"Vorige %d dagen\"}";
$a["times||count"] = "{\"one\":\"%d keer\", \"other\":\"%d keer\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d-dag reeks\", \"other\":\"%d-dag reeks\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ van %1d gewoonte voltooid\", \"other\":\"%2#@completed@ van %1d gewoonten voltooid\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d-dag reeks\", \"other\":\"%d-dag reeks\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d-dag reeks\", \"other\":\"%d-dag reeks\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d gewoonte\", \"other\":\"%d gewoonten\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d gewoonte\", \"other\":\"%d gewoonten\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d gewoonte\", \"other\":\"%d gewoonten\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d gewoonte\", \"other\":\"%d gewoonten\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d gewoonte\", \"other\":\"%d gewoonten\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d-dag reeks\", \"other\":\"%d-dag reeks\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d gewoonte\", \"other\":\"%d gewoonten\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d gewoonte\", \"other\":\"%d gewoonten\"}";

return $a;
?>