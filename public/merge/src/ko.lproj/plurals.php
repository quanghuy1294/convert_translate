<?php
$a = [];
$a["day.per.week||count"] = "{\"other\":\"주 %d일\"}";
$a["streak||count"] = "{\"other\":\"%d회 연속 달성\"}";
$a["more||count"] = "{\"other\":\"%d 개 더보기\"}";
$a["time.of.day.notification.group||notifications"] = "{\"other\":\"%@에 %u개의 습관 남음\"}";
$a["interval.day||count"] = "{\"other\":\"%d일 마다\"}";
$a["streak.day||count"] = "{\"other\":\"%d일 연속 달성!\"}";
$a["month||count"] = "{\"other\":\"%d개월\"}";
$a["passed.week||count"] = "{\"other\":\"지난 %d주\"}";
$a["passed.days||count"] = "{\"other\":\"최근 %d일\"}";
$a["times||count"] = "{\"other\":\"%d회\"}";
$a["streak.day.journal||count"] = "{\"other\":\"%d일 연속 달성\"}";
$a["habit.remaining||required"] = "{\"other\":\"습관 %1d개 중 %2#@completed@개 완료함\"}";
$a["habit.remaining||completed"] = "{\"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"other\":\"%d일 연속 달성\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"other\":\"%d일 연속 달성\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"other\":\"%d개의 습관\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"other\":\"%d개의 습관\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"other\":\"%d개의 습관\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"other\":\"%d개의 습관\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"other\":\"%d개의 습관\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"other\":\"%d일 연속 달성\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"other\":\"%d개의 습관\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"other\":\"%d개의 습관\"}";

return $a;
?>