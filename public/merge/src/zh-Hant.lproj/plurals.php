<?php
$a = [];
$a["day.per.week||count"] = "{\"other\":\"每週 %d 天\"}";
$a["streak||count"] = "{\"other\":\"連續完成天數：%d\"}";
$a["more||count"] = "{\"other\":\"再來%d個\"}";
$a["time.of.day.notification.group||notifications"] = "{\"other\":\"%@還有 %u 個習慣\"}";
$a["interval.day||count"] = "{\"other\":\"每 %d 天\"}";
$a["streak.day||count"] = "{\"other\":\"連續完成 %d 天！\"}";
$a["month||count"] = "{\"other\":\"%d 個月\"}";
$a["passed.week||count"] = "{\"other\":\"前%d 周\"}";
$a["passed.days||count"] = "{\"other\":\"前 %d 天\"}";
$a["times||count"] = "{\"other\":\"%d 次\"}";
$a["streak.day.journal||count"] = "{\"other\":\"%d 天連續完成 ！\"}";
$a["habit.remaining||required"] = "{\"other\":\"%1d個習慣中已完成%2#@completed@ 個\"}";
$a["habit.remaining||completed"] = "{\"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"other\":\"%d 天連續完成 ！\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"other\":\"%d 天連續完成 ！\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"other\":\"%d習慣\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"other\":\"%d習慣\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"other\":\"%d習慣\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"other\":\"%d個習慣\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"other\":\"%d習慣\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"other\":\"%d 天連續完成 ！\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"other\":\"%d習慣\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"other\":\"%d習慣\"}";

return $a;
?>