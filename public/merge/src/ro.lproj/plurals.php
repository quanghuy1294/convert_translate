<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d zi pe săptămână\", \"few\":\"%d zile pe săptămână\", \"other\":\"%d zile pe săptămână\"}";
$a["streak||count"] = "{\"one\":\"%d Combo\", \"few\":\"%d Combo-uri\", \"other\":\"%d Combo-uri\"}";
$a["more||count"] = "{\"one\":\"încă %d\", \"few\":\"încă %d\", \"other\":\"încă %d\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u obicei rămas în %@\", \"few\":\"%u obiceiuri rămase în %@\", \"other\":\"%u obiceiuri rămase în %@\"}";
$a["interval.day||count"] = "{\"one\":\"Zilnic\", \"few\":\"La fiecare %d zile\", \"other\":\"La fiecare %d zile\"}";
$a["streak.day||count"] = "{\"one\":\"%d zi combo!\", \"few\":\"%d zile combo!\", \"other\":\"%d zile combo!\"}";
$a["month||count"] = "{\"one\":\"%d lună\", \"few\":\"%d luni\", \"other\":\"%d luni\"}";
$a["passed.week||count"] = "{\"one\":\"Ultima săptămâna\", \"few\":\"Ultimele %d săptămâni\", \"other\":\"Ultimele %d săptămâni\"}";
$a["passed.days||count"] = "{\"one\":\"Ultima zi\", \"few\":\"Ultimele %d zile\", \"other\":\"Ultimele %d zile\"}";
$a["times||count"] = "{\"one\":\"%d dată\", \"few\":\"de %d ori\", \"other\":\"de %d ori\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d zi combo\", \"few\":\"%d zile combo\", \"other\":\"%d zile combo\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ din %1d obicei completat\", \"few\":\"%2#@completed@ din %1d obiceiuri completate\", \"other\":\"%2#@completed@ din %1d obiceiuri completate\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"few\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d-zi combo\", \"few\":\"%d-zile combo\", \"other\":\"%d-zile combo\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d-zi combo\", \"few\":\"%d-zile combo\", \"other\":\"%d-zile combo\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d obicei\", \"few\":\"%d obiceiuri\", \"other\":\"%d obiceiuri\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d obicei\", \"few\":\"%d obiceiuri\", \"other\":\"%d obiceiuri\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d obicei\", \"few\":\"%d obiceiuri\", \"other\":\"%d obiceiuri\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d obicei\", \"few\":\"%d obiceiuri\", \"other\":\"%d obiceiuri\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d obicei\", \"few\":\"%d obiceiuri\", \"other\":\"%d obiceiuri\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d-zi combo\", \"few\":\"%d-zile combo\", \"other\":\"%d-zile combo\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d obicei\", \"few\":\"%d obiceiuri\", \"other\":\"%d obiceiuri\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d obicei\", \"few\":\"%d obiceiuri\", \"other\":\"%d obiceiuri\"}";

return $a;
?>