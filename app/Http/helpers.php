<?php
const LANG_BASE = 'base';
const LANG_EN = 'en';
const LANG_JA = 'ja';
const LANG_CN = 'cn';
const LANG_RU = 'ru';
const LANG_FR = 'fr';
const LANG_SP = 'sp';
const LANG_SPL = 'spl';
const LANG_SPM = 'spm';
const LANG_PO = 'po';
const LANG_POL = 'pol';
const LANG_POB = 'pob';
const LANG_GM = 'gm';
const LANG_CNT = 'cnt';
const LANG_DU = 'du';
const LANG_IT = 'it';
const LANG_KR = 'kr';
const LANG_SW = 'sw';
const LANG_VN = 'vn';

function list_lang () {
    return [
        LANG_BASE => 'Base',
        LANG_EN => 'English',
        LANG_JA => 'Japanese',
        LANG_CN => 'Chinese (Simplified)',
        LANG_RU => 'Rusian',
        LANG_FR => 'French',
        LANG_SP => 'Spanish',
        LANG_SPL => 'Spanish (Latin America)',
        LANG_SPM => 'Spanish (Mexico)',
        LANG_PO => 'Portuguese (Potugal)',
        LANG_POB => 'Portuguese (Brazil)',
        LANG_POL => 'Polish',
        LANG_GM => 'Germany',
        LANG_CNT => 'Chinese (Traditional)',
        LANG_DU => 'Dutch',
        LANG_IT => 'Italian',
        LANG_KR => 'Korean',
        LANG_SW => 'Swedish',
        LANG_VN => 'Vietnamese',
    ];
}

function list_lang_write () {
    return [
        LANG_EN => 'English',
        LANG_JA => 'Japanese',
        LANG_CN => 'Chinese (Simplified)',
        LANG_RU => 'Rusian',
        LANG_FR => 'French',
        LANG_SP => 'Spanish',
        LANG_SPL => 'Spanish (Latin America)',
        LANG_SPM => 'Spanish (Mexico)',
        LANG_PO => 'Portuguese (Potugal)',
        LANG_POB => 'Portuguese (Brazil)',
        LANG_POL => 'Polish',
        LANG_GM => 'Germany',
        LANG_CNT => 'Chinese (Traditional)',
        LANG_DU => 'Dutch',
        LANG_IT => 'Italian',
        LANG_KR => 'Korean',
        LANG_SW => 'Swedish',
        LANG_VN => 'Vietnamese',
    ];
}

function clear_string ($str, $value = false) {
    $newStr = $str;
    if ($value) {
        $tmpValues = explode(';', $str);
        $newStr = $tmpValues[0];
    }

    $tmpStr = $newStr;
    if (strpos($newStr, '"') <= 0)
        $tmpStr = str_replace_first('"', "", $newStr);
    
    $tmpStr = str_replace_last('";', "", $tmpStr);
    $tmpStr = str_replace_last('"', "", $tmpStr);

    $tmpStr = str_replace('%d', "{num}", $tmpStr);
    $tmpStr = str_replace('%@', "{msg}", $tmpStr);
    $tmpStr = str_replace('%u', "{u1}", $tmpStr);
    $tmpStr = str_replace('%1$@', "{msg1}", $tmpStr);
    $tmpStr = str_replace('%2$@', "{msg2}", $tmpStr);
    $tmpStr = str_replace('%3$@', "{msg3}", $tmpStr);

    // replace plural

    $tmpStr = trim($tmpStr);

    return $tmpStr;
}

function replace_plural_string ($values) {
    $keys = array_keys($values);
    $tmpLess = $values[$keys[0]];
    $tmpMore = $values[$keys[count($keys) - 1]];

    // $replacer = ['%#@count@', '%#@notifications@', '%#@streak@', '%2$#@required@', '%#@habit_count@', '%2$#@streak_day_count@', '%1$#@pending@', '%2$#@completed@', '%1$#@required@'];
    // foreach($replacer as $replace) {
    //     $tmpLess = str_replace($replace, $less, $tmpLess);
    //     $tmpMore = str_replace($replace, $more, $tmpMore);
    // }
    $tmpLess = str_replace(PHP_EOL, '', $tmpLess);
    $tmpMore = str_replace(PHP_EOL, '', $tmpMore);

    $replacerNormal = [
        '{num}' => '%d', 
        '{num1}' => '%1$d',
        '{num2}' => '%2$d',

        '{num1}' => '%1d',
        '{num2}' => '%2d',

        "{msg}" => "%@",
        "{msg1}" => '%1$@',
        "{msg2}" => '%2$@',
        "{msg3}" => '%3$@',

        "{u1}" => "%u",
    ];
    foreach($replacerNormal as $sig => $replace) {
        $tmpLess = str_replace($replace, $sig, $tmpLess);
        $tmpMore = str_replace($replace, $sig, $tmpMore);
        
    }
    

    $tmpStr = implode(' | ', [$tmpLess, $tmpMore]);

    return $tmpStr;
}

function replace_plural_string_dict ($str, $values) {
    $keys = array_keys($values);
    // replace plural
    $less = $values[$keys[0]];

    $tmpLess = $str;

    $replacer = ['%#@count@', '%#@notifications@', '%#@streak@', '%2$#@required@', '%#@habit_count@', '%2$#@streak_day_count@', '%1$#@pending@', '%2$#@completed@', '%1$#@required@', '%2#@completed@'];
    foreach($replacer as $replace) {
        $tmpLess = str_replace($replace, $less, $tmpLess);
    }

    $replacerNormal = [
        '{num}' => '%d', 
        '{num1}' => '%1$d',
        '{num2}' => '%2$d',

        '{num1}' => '%1d',
        '{num2}' => '%2d',

        "{msg}" => "%@",
        "{msg1}" => '%1$@',
        "{msg2}" => '%2$@',
        "{msg3}" => '%3$@',

        "{u1}" => "%u",
    ];
    foreach($replacerNormal as $sig => $replace) {
        $tmpLess = str_replace($replace, $sig, $tmpLess);
    }
    

    return $tmpLess;
}

function replace_plural_for_zero ($str) {
    $items = explode('|', $str);
    $zero = $items[0];

    $replacerNormal = [
        '{num}' => 0, 
        '{num} of {num1}' => 0,
        '{num1}' => 0,

        "{u1}" => "no",
    ];

    // if (strpos($zero, '{num}') !== false && strpos($zero, '{num1}') !== false) {
    //     $last = strpos($zero, '{num1}');
    //     dump($zero . ' - ' . $last);
    //     $zero = substr($zero, ($last - strlen($zero)));
    // }

    foreach($replacerNormal as $sig => $replace) {
        $zero = str_replace($sig, $replace, $zero);
        
    }
    
    return trim($zero, " ");
}

