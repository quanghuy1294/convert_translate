<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d dag per vecka\", \"other\":\"%d dagar per vecka\"}";
$a["streak||count"] = "{\"one\":\"%d Streak\", \"other\":\"%d Streaks\"}";
$a["more||count"] = "{\"one\":\"%d mer\", \"other\":\"%d mer\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u till vana på %@\", \"other\":\"%u till vanor på %@\"}";
$a["interval.day||count"] = "{\"one\":\"Var %d dag\", \"other\":\"Var %d dag\"}";
$a["streak.day||count"] = "{\"one\":\"%d dag i sträck!\", \"other\":\"%d dagar i sträck!\"}";
$a["month||count"] = "{\"one\":\"%d månad\", \"other\":\"%d månader\"}";
$a["passed.week||count"] = "{\"one\":\"Sista veckan\", \"other\":\"Senaste %d veckorna\"}";
$a["passed.days||count"] = "{\"one\":\"Senaste dagen\", \"other\":\"Senaste %d dagarna\"}";
$a["times||count"] = "{\"one\":\"%d gång\", \"other\":\"%d gånger\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d-dag i sträck\", \"other\":\"%d-dagar i sträck\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ av %1d vana slutförd\", \"other\":\"%2#@completed@ av %1d vanor slutförda\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d-dag i sträck\", \"other\":\"%d-dag i sträck\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d-dag i sträck\", \"other\":\"%d-dag i sträck\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d vana\", \"other\":\"%d vanor\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d vana\", \"other\":\"%d vanor\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d vana\", \"other\":\"%d vanor\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d vana\", \"other\":\"%d vanor\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d vana\", \"other\":\"%d vanor\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d-dag i sträck\", \"other\":\"%d-dag i sträck\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d vana\", \"other\":\"%d vanor\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d vana\", \"other\":\"%d vanor\"}";

return $a;
?>