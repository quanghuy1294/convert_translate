<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d day per week\", \"other\":\"%d days per week\"}";
$a["streak||count"] = "{\"one\":\"%d Streak\", \"other\":\"%d Streaks\"}";
$a["more||count"] = "{\"one\":\"%d more\", \"other\":\"%d more\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u more habit in %@\", \"other\":\"%u more habits in %@\"}";
$a["interval.day||count"] = "{\"one\":\"Every %d day\", \"other\":\"Every %d days\"}";
$a["streak.day||count"] = "{\"one\":\"%d day streaks!\", \"other\":\"%d days streaks!\"}";
$a["month||count"] = "{\"one\":\"%d month\", \"other\":\"%d months\"}";
$a["passed.week||count"] = "{\"one\":\"Last %d week\", \"other\":\"Last %d weeks\"}";
$a["passed.days||count"] = "{\"one\":\"Last %d day\", \"other\":\"Last %d days\"}";
$a["times||count"] = "{\"one\":\"%d time\", \"other\":\"%d times\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ of %1d habit completed\", \"other\":\"%2#@completed@ of %1d habits completed\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d habit\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d habit\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d habit\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d habit\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d habit\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d habit\", \"other\":\"%d habits\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d habit\", \"other\":\"%d habits\"}";

return $a;
?>