<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d dia a la setmana\", \"other\":\"%d dies a la setmana\"}";
$a["streak||count"] = "{\"one\":\"%d ratxa\", \"other\":\"%d ratxes\"}";
$a["more||count"] = "{\"one\":\"%d més\", \"other\":\"%d més\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u hàbit més en %@\", \"other\":\"%u hàbits més en %@\"}";
$a["interval.day||count"] = "{\"one\":\"Cada dia\", \"other\":\"Cada %d dies\"}";
$a["streak.day||count"] = "{\"one\":\"Ratxa de %d dia!\", \"other\":\"Ratxa de %d dies!\"}";
$a["month||count"] = "{\"one\":\"%d mes\", \"other\":\"%d mesos\"}";
$a["passed.week||count"] = "{\"one\":\"Última setmana\", \"other\":\"Últimes %d setmanes\"}";
$a["passed.days||count"] = "{\"one\":\"Últim dia\", \"other\":\"Últims %d dies\"}";
$a["times||count"] = "{\"one\":\"%d vegada\", \"other\":\"%d vegades\"}";
$a["streak.day.journal||count"] = "{\"one\":\"Ratxa de un dia\", \"other\":\"Ratxa de %d dies\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ de %1d hàbit completat\", \"other\":\"%2#@completed@ de %1d hàbits completats\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"Ratxa de un dia\", \"other\":\"Ratxa de %d dies\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"Ratxa de un dia\", \"other\":\"Ratxa de %d dies\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"Un hàbit\", \"other\":\"%d hàbits\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"Un hábit\", \"other\":\"%d hàbits\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"Un hàbit\", \"other\":\"%d hàbits\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"Un hábit\", \"other\":\"%d hàbits\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"Un hábit\", \"other\":\"%d hàbits\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"Ratxa de un dia\", \"other\":\"Ratxa de %d dies\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"Un hàbit\", \"other\":\"%d hàbits\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"Un hábit\", \"other\":\"%d hàbits\"}";

return $a;
?>