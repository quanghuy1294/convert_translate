<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d day per week\", \"two\":\"%d days per week\", \"many\":\"%d days per week\", \"other\":\"%d days per week\"}";
$a["streak||count"] = "{\"one\":\"%d Streak\", \"two\":\"%d Streaks\", \"many\":\"%d Streaks\", \"other\":\"%d Streaks\"}";
$a["more||count"] = "{\"one\":\"%d more\", \"two\":\"%d more\", \"many\":\"%d more\", \"other\":\"%d more\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u more habit in %@\", \"two\":\"%u more habits in %@\", \"many\":\"%u more habits in %@\", \"other\":\"%u more habits in %@\"}";
$a["interval.day||count"] = "{\"one\":\"Every %d day\", \"two\":\"Every %d days\", \"many\":\"Every %d days\", \"other\":\"Every %d days\"}";
$a["streak.day||count"] = "{\"one\":\"%d day streaks!\", \"two\":\"%d days streaks!\", \"many\":\"%d days streaks!\", \"other\":\"%d days streaks!\"}";
$a["month||count"] = "{\"one\":\"%d month\", \"two\":\"%d months\", \"many\":\"%d months\", \"other\":\"%d months\"}";
$a["passed.week||count"] = "{\"one\":\"Last %d week\", \"two\":\"Last %d weeks\", \"many\":\"Last %d weeks\", \"other\":\"Last %d weeks\"}";
$a["passed.days||count"] = "{\"one\":\"Last %d day\", \"two\":\"Last %d days\", \"many\":\"Last %d days\", \"other\":\"Last %d days\"}";
$a["times||count"] = "{\"one\":\"%d time\", \"two\":\"%d times\", \"many\":\"%d times\", \"other\":\"%d times\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d-day streak\", \"two\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ of %1d habit completed\", \"two\":\"%2#@completed@ of %1d habits completed\", \"many\":\"%2#@completed@ of %1d habits completed\", \"other\":\"%2#@completed@ of %1d habits completed\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"two\":\"%d\", \"many\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d-day streak\", \"two\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d-day streak\", \"two\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d habit\", \"two\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d habit\", \"two\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d habit\", \"two\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d habit\", \"two\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d habit\", \"two\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d-day streak\", \"two\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d habit\", \"two\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d habit\", \"two\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";

return $a;
?>