<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Original;
use App\Models\Translation;
use App\Models\Template;
use App\Models\TemplateDetail;
use Illuminate\Support\Facades\File;

use Nathanmac\Utilities\Parser\Parser;
use Spatie\ArrayToXml\ArrayToXml;

// use Orchestra\Parser\Xml\Facade as XmlParser;
// use Nathanmac\Utilities\Parser\Facades\Parser;

class ImportOriginalController extends Controller
{
    protected $path = 'Localizable.strings';

    public function index() {
        $content = file_get_contents(public_path('originalstr/' . $this->path));
        $this->import($content);
    }

    public function import ($content) {
        $listKV = explode(PHP_EOL, $content);
        
        foreach ($listKV as $line) {
            try {
                if (empty($line))
                    continue;
                $split = explode("=", $line);
                if (count($split) <= 1)
                    continue;

                list($key, $value) = $split;
                $key = clear_string($key);
                $value = clear_string($value, true);

                $data = [
                    'key' => $key,
                    'value' => $value,
                ];
                Original::create($data);
            } catch (\Exception $exception) {
                dump($line . ' - ' . $exception->getMessage());
            }
        }
    }

    public function importLang () {
        return view('importLang');
    }

    public function store(Request $request) {
        $file = $request->file('file');
        $lang = $request->get('lang');
        $content = file_get_contents($file);
        
        $this->updateLang($lang, $content);
    }

    public function updateLang($lang, $content) {
        $listKV = explode(PHP_EOL, $content);
        $original = Original::all();
        foreach ($listKV as $line) {
            try {
                if (empty($line))
                    continue;
                $split = explode("=", $line);
                if (count($split) <= 1)
                    continue;

                list($key, $value) = $split;
                $key = clear_string($key);
                $value = clear_string($value, true);

                $og = $original->where('key', $key)->first();
                $keyId = $og ? $og->id : 0;
                $data = [
                    'key_id' => $keyId,
                    'lang' => $lang,
                    'translate' => $value,
                    'new_key' => $key,
                ];
                Translation::create($data);
            } catch (\Exception $exception) {
                dump($line . ' - ' . $exception->getMessage(), $exception->getLine());
            }
        }
    }

    public function importTemplate () {
        return view('importTemplate');
    }

    public function storeTemplate(Request $request) {
        $files = $request->file('file');
        $name = $files[0]->getClientOriginalName();
        $content = file_get_contents($files[0]);
        foreach($files as $file) {
            $this->storeDataTemplate($file);
        }

        //return redirect()->route('import.importTemplate');
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     */
    public function storeDataTemplate ($file) {
        $name = $file->getClientOriginalName();
        $content = file_get_contents($file);
        $template = new Template();
        $template->fill([
            'name' => $name
        ])->save();

        $listKV = explode(PHP_EOL, $content);
        foreach ($listKV as $line) {
            try {
                if (empty($line))
                    continue;
                $split = explode("=", $line);
                if (count($split) <= 1)
                    continue;

                list($key, $value) = $split;
                $key = clear_string($key);
                $value = clear_string($value, true);

                $data = [
                    'template_id' => $template->id,
                    'key' => $key,
                    'value' => $value,
                ];
                TemplateDetail::create($data);
            } catch (\Exception $exception) {
                dump($line . ' - ' . $exception->getMessage(), $exception->getLine());
            }
        }
    }

    public function generateFiles () {
        return view('generateFiles');
    }

    public function generator() {
        $langs = array_keys(list_lang_write());
        $listLang = list_lang_write();
        $root = 'generatorResources';
        $templates = Template::with('detail')->get();
        $zipFileName = 'Converted.zip';
        $zip = new \ZipArchive();
        $zipFile = public_path($root) . '/archive.zip';
        $res = $zip->open($zipFile, \ZipArchive::CREATE);
        $notFound = [];

        foreach($langs as $lang) {
            $translations = Translation::query()->where('lang', $lang)->get();
            $newPath = $root . '/' . $lang;
            if (File::isDirectory($newPath)) {
                File::deleteDirectory(public_path($newPath));
            }
            File::makeDirectory(public_path($newPath), 0777, true);

            foreach($templates as $template) {
                $templateData =  $template->detail->pluck('value', 'key')->toArray();
                $filePath = $newPath . '/' . $template->name;
                $notFound[] = "Scan tempate: {$template->name} - language: {$listLang[$lang]}";
                
                $templateContent = [];
                foreach($templateData as $k => $v) {
                    $originText = Original::query()->where('value', $v)->first();
                    
                    $tmp = implode('', ["\"$k\"", " = ", "\"$v\"", ";", " // not found key: " . $k]);
                    $notfoundFlag = true;
                    if ($originText) {
                        $tranText = $translations->where('key_id', $originText->id)->where('lang', $lang)->first();
                        if ($tranText) {
                            $tmp = implode('', ["\"$k\"", " = ", "\"$tranText->translate\"", ";"]);
                            $notfoundFlag = false;
                        }
                    }
                    if ($notfoundFlag) {
                        $notFound[] = $tmp;
                    }

                    $templateContent[] = $tmp;
                }
                file_put_contents(public_path($filePath), implode(PHP_EOL, $templateContent));
                $zip->addFile(public_path($filePath), $listLang[$lang] . '/' . $template->name);
            }
            $notFound[] = '__________________';
        }
        file_put_contents(public_path($root . '/notfoundText.strings'), implode(PHP_EOL, $notFound));
        $zip->close();
    }

    public function generatorPolish() {
        $langs = array_keys([LANG_POL => 'Polish']);
        $listLang = [LANG_POL => 'Polish'];
        $root = 'generatorResources';
        $templates = Template::with('detail')->get();
        // $zipFileName = 'Converted.zip';
        // $zip = new \ZipArchive();
        // $zipFile = public_path($root) . '/archive.zip';
        // $res = $zip->open($zipFile, \ZipArchive::CREATE);
        // $notFound = [];

        foreach($langs as $lang) {
            $translations = Translation::query()->where('lang', $lang)->get();
            $newPath = $root . '/' . $lang;
            if (File::isDirectory($newPath)) {
                File::deleteDirectory(public_path($newPath));
            }
            File::makeDirectory(public_path($newPath), 0777, true);

            foreach($templates as $template) {
                dump('---------->' . $template->name);
                $templateData =  $template->detail->pluck('value', 'key')->toArray();
                $filePath = $newPath . '/' . $template->name;
                
                $templateContent = [];
                foreach($templateData as $k => $v) {
                    dump($k . ' - ' . $v);
                    $originText = Original::query()->where('value', $v)->first();
                    
                    $tmp = implode('', ["\"$k\"", " = ", "\"$v\"", ";", " // not found key: " . $k]);
                    if ($originText) {
                        $tranText = $translations->where('key_id', $originText->id)->where('lang', $lang)->first();
                        if ($tranText) {
                            $tmp = implode('', ["\"$k\"", " = ", "\"$tranText->translate\"", ";"]);
                        }
                    }

                    $templateContent[] = $tmp;
                }
                file_put_contents(public_path($filePath), implode(PHP_EOL, $templateContent));
            }
        }
    }

    public function readXliff() {
        $path = public_path('xliff/source/de.xliff');
        $content = file_get_contents($path);

        $parser = new Parser();
        $data = $parser->xml($content);

        $files = $data['file'];
        $duplicate = [];
        foreach ($files as $key => $file) {
            $trans = $file['body']['trans-unit'];
            foreach($trans as $unit) {
                $trans = !empty($unit["@id"]) ? $unit["@id"] : null;
                if (empty($trans))
                    continue;
                if (empty($duplicate[$trans])) {
                    $duplicate[$trans] = 1;
                } else {
                    $duplicate[$trans] += 1;
                }
            }
        }

        $filtered = array_filter($duplicate, function($value) {
            return $value > 1;
        });

        $newData = $files;
        // dump($newData);
        $fileDataResult = [];
        foreach ($newData as $key => $file) {
            $newFile = [
                '_attributes' => [],
                'body' => [
                    'trans-unit' => []
                ]
            ];
            foreach($file as $fileKey => $fileValue) {
                if (strpos($fileKey, "@") !== false) {
                    $name = str_replace("@", "", $fileKey);
                    $newFile['_attributes'][$name] = $fileValue; 
                } else if ($fileKey == 'header') {
                    foreach ($fileValue['tool'] as $headerKey => $headerValue) {
                        $name = str_replace("@", "", $headerKey);
                        $newFile['header']['tool']['_attributes'][$name] = $headerValue; 
                    }
                } else if ($fileKey == 'body') {
                    if (!empty($fileValue['trans-unit']['@id'])) {
                        foreach($fileValue['trans-unit'] as $unitKey => $unitChild) {
                            $unitData = [];
                            if (strpos($unitKey, "@") !== false) {
                                $name = str_replace("@", "", $unitKey);
                                $unitData['_attributes'][$name] = $unitChild;
                            } else {
                                $unitData[$unitKey] = $unitChild;
                            }
                            
                            $newFile['body']['trans-unit'][] = $unitData;
                        }
                    } else {
                        foreach($fileValue['trans-unit'] as $unitKey => $unitChild) {
                            $unitData = [];
                            try {
                                foreach ($unitChild as $unitKey => $unitValue) {
                                    if (strpos($unitKey, "@") !== false) {
                                        $name = $unitValue;
                                        if ($unitKey === "@id" && !empty($filtered[$unitValue])) {
                                            $fileName = explode('/', $newFile['_attributes']['original']);
                                            $fileNames = explode('.', $fileName[count($fileName) - 1]);
                                            $fileName = $fileNames[0];
                                            // dump($fileName);
                                            $name = $fileName . '.' . $unitValue;
                                        }

                                        $nameKey = str_replace("@", "", $unitKey);
                                        $unitData['_attributes'][$nameKey] = strtolower($name);
                                    } else {
                                        $unitData[$unitKey] = $unitValue;
                                    }
                                }
                                $newFile['body']['trans-unit'][] = $unitData;
                            } catch (\Exception $exception) {
                                dump($exception->getMessage(), $exception, $unitKey, $unitChild, $fileValue, count($fileValue), '____');
                            }
                        }
                    }
                }
            }
            $fileName = explode('/', $newFile['_attributes']['original']);
            $fileNames = explode('.', $fileName[count($fileName) - 1]);
            $fileName = $fileNames[0];
            if ($fileName == 'Manage' || $fileName == 'Journal') 
                $fileDataResult['file'][] = $newFile;
        }

        dd($filtered, $fileDataResult);

        $result = ArrayToXml::convert($fileDataResult, 
        [
            'rootElementName' => 'xliff',
            '_attributes' => [
                'xmlns' => "urn:oasis:names:tc:xliff:document:1.2",
                'xmlns:xsi' => "http://www.w3.org/2001/XMLSchema-instance",
                'version' => "1.2",
                'xsi:schemaLocation' => "urn:oasis:names:tc:xliff:document:1.2 http://docs.oasis-open.org/xliff/v1.2/os/xliff-core-1.2-strict.xsd"
            ],
        ], true, 'UTF-8');

        file_put_contents(public_path('xliff/gen/de.xliff'), $result);
    }

    public function mergeForm() {
        return view('mergeForm');
    }

    public function mergeProcess(Request $request) {
        $ignores = [
            '3DTouchShortcut.strings' => '3DTouchShortcut.strings', 
            'AppleWatch.strings' => 'AppleWatch.strings', 
            'Intents.strings' => 'Intents.strings', 
            'Plurals.stringsdict' => 'Plurals.stringsdict',
        ];
        
        $folders = array_filter(glob(public_path('merge/src/*')), 'is_dir');
        $quotes = array_filter(glob(public_path('merge/quote/*')), 'is_dir');

        // $folders = ['/Applications/XAMPP/xamppfiles/htdocs/unstatic.co/convert-trans/public/merge/src/vi.lproj'];
        
        foreach ($folders as $folder) {
            $files = glob($folder . '/*.strings');
            
            $fileNames = explode('/', $folder);
            $latName = $fileNames[count($fileNames) - 1];
            $type = 'json';
            $separate = $type === 'strings' ? '=' : ':';
            $symbolEnd = $type === 'strings' ? ';' : ',';
            $ext = $type === 'strings' ? '.strings' : '.json';

            $newfileName = substr($latName, 0, strpos($latName, '.')) . $ext;
            $newFilePath = public_path('merge/generate/' . $newfileName);
            // dump($newFilePath, File::exists($newFilePath));
            if (File::exists($newFilePath))
                File::delete($newFilePath);

            if ($type === 'json')
                File::prepend($newFilePath, '{'. PHP_EOL);
            $lineCount = 0;
            foreach ($files as $k => $file) {
                $content = file_get_contents($file);
                $fileName = str_replace($folder . '/', '', $file);
                $prefix = strtolower(str_replace('.strings', '', $fileName));
                if (!empty($ignores[$fileName]))
                    continue;
    
                
                $listKV = explode(PHP_EOL, $content);
                foreach ($listKV as $line) {
                    try {
                        if (empty($line))
                            continue;
                        $split = explode("\" = \"", $line);
                        if (count($split) <= 1)
                            continue;
    
                        list($key, $value) = $split;
                        $key = clear_string($key);
                        $value = clear_string($value, true);
                        // dump($lineCount, $lineCount > 0);
                        if ($lineCount)
                            File::append($newFilePath, $symbolEnd . PHP_EOL);
                        File::append($newFilePath, '"' . $prefix . '.' . $key . '" ' . $separate . ' "' . $value . '"');
                        $lineCount++;
                        
                    } catch (\Exception $exception) {
                        dump('error: ');
                        dump($line . ' - ' . $exception->getMessage(), $exception->getLine());
                    }
                }
            }
        }
        
        foreach ($folders as $folder) {
            $files = glob($folder . '/*.stringsdict');
            foreach ($files as $k => $file) {
                $content = file_get_contents($file);
                $fileNamePrefix = str_replace($folder . '/', '', $file);
                $prefix = strtolower(str_replace('.stringsdict', '', $fileNamePrefix));
                

                $fileNames = explode('/', $folder);
                $latName = $fileNames[count($fileNames) - 1];
                $type = 'json';
                $separate = $type === 'strings' ? '=' : ':';
                $symbolEnd = $type === 'strings' ? ';' : ',';
                $ext = $type === 'strings' ? '.strings' : '.json';

                $newfileName = substr($latName, 0, strpos($latName, '.')) . $ext;
                $newFilePath = public_path('merge/generate/' . $newfileName);
                
                $this->convertFilePlural($file, $folder, $prefix);
                $lines = $this->readPlural($folder . '/' . $prefix .'.php');
                
                foreach ($lines as $key =>$value) {
                    $item0 = replace_plural_for_zero($value);
                    $value = $item0 . ' | ' . $value;
                    File::append($newFilePath, $symbolEnd . PHP_EOL);
                    File::append($newFilePath, '"' . $prefix . '.' . $key . '" ' . $separate . ' "' . $value . '"');
                }
            }
            // if ($type === 'json')
            //     File::append($newFilePath, PHP_EOL . '}');
        }

        $this->convertFileQuote($quotes);
    }

    public function convertFilePlural ($file, $path, $name) {
        $client = new \GuzzleHttp\Client();
        $filePath = $path . '/' . $name .'.php';
        if (File::exists($filePath))
            File::delete($filePath);

        $response = $client->request('POST', 'http://converter.webtranslateit.com//converter?convert_to=PhpArrayList', [
            'multipart' => [
                [
                    'name'     => 'convert_to',
                    'contents' => 'PhpArrayList'
                ],
                [
                    'name'     => 'file',
                    'contents' => fopen($file, 'r')
                ],
            ]
        ]);

        $body = $response->getBody();
        $newContent = "<?php\n\$a = [];\n";
        $newBody = $body->getContents();
        $newBody = str_replace("%1\$", "%1", $newBody);
        $newBody = str_replace("%2\$\\", "%2", $newBody);
        $newBody = str_replace("\$[", "\$a[", $newBody);
        $newBody = str_replace("=>", ":", $newBody);
        $newBody = str_replace("?>", "", $newBody);

        file_put_contents($filePath, $newContent, FILE_APPEND);
        file_put_contents($filePath, $newBody, FILE_APPEND);
        file_put_contents($filePath, "return \$a;\n?>", FILE_APPEND);
    }

    public function readPlural($path) {
        $content = require($path);
        $result = [];
        foreach ($content as $key => $row) {
            $ignoreKey = [
                'daily.briefing',
                'habit.notification'
            ];
            $flagKey = true;
            foreach ($ignoreKey as $ignore) {
                if (strpos($key, $ignore) >= 0 && strpos($key, $ignore) !== false){
                    $flagKey = false;
                }
            }

            if (!$flagKey) {
                continue;
            }

            $keys = explode('||', $key);
            $acceptKey = $keys[0];
            $decode = (array) json_decode($row);
            $value = !empty($result[$acceptKey]) ? $result[$acceptKey] . $row : null;
            if (empty($result[$acceptKey])) {
                $values = (array) json_decode($row);
                try {
                    $value = replace_plural_string($values);
                    $result[$acceptKey] = $value;
                } catch (\Exception $exception) {
                    dump($key . 'error', $exception->getMessage(), $values, $row, json_decode($row));
                }
                
            } else {
                $result[$acceptKey] = replace_plural_string_dict($result[$acceptKey], $decode);
            }            
        }

        return $result;
    }

    public function convertFileQuote($quotes, $filetype = 'json') {
        $separate = $filetype === 'strings' ? '=' : ':';
        $symbolEnd = $filetype === 'strings' ? ';' : ',';
        $ext = $filetype === 'strings' ? '.strings' : '.json';
        foreach ($quotes as $quote) {
            $fileNames = explode('/', $quote);
            $latName = $fileNames[count($fileNames) - 1];

            $newfileName = substr($latName, 0, strpos($latName, '.')) . $ext;
            $newFilePath = public_path('merge/generate/' . $newfileName);
            dump('quote: ' . $newFilePath);
            
            $file = glob($quote . '/Quotes.plist')[0];
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'http://converter.webtranslateit.com//converter?convert_to=Json', [
                'multipart' => [
                    [
                        'name'     => 'convert_to',
                        'contents' => 'Json'
                    ],
                    [
                        'name'     => 'file',
                        'contents' => fopen($file, 'r')
                    ],
                ]
            ]);
            $body = (array) json_decode($response->getBody()->getContents());
            foreach ($body as $key => $value) {
                $matches = null;
                $re = '/\/plist\/array\/dict\[(\d+)]\/string\[(\d+)\]/m';
                
                preg_match_all($re, $key, $matches, PREG_SET_ORDER, 0);
                $index = $matches[0][1];
                $type = $matches[0][2];

                
                File::append($newFilePath, $symbolEnd . PHP_EOL);
                    
                $value = trim(preg_replace('/\t+/', '', $value));
                if ($type == 1)
                    $_key = 'quote.widget.' . $index . '.msg';
                else
                    $_key = 'quote.widget.' . $index . '.auth';

                // dump($_key . $separate . ' "' . $value . '"');
                File::append($newFilePath, '"' .$_key . '" ' . $separate . ' "' . $value . '"');
            }

            if ($filetype === 'json')
                File::append($newFilePath, PHP_EOL . '}');
            
        }
    }
}
