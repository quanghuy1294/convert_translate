<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d día por semana\", \"other\":\"%d días por semana\"}";
$a["streak||count"] = "{\"one\":\"Racha %d día\", \"other\":\"Racha %d días\"}";
$a["more||count"] = "{\"one\":\"%d más\", \"other\":\"%d más\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u hábito más en %@\", \"other\":\"%u hábitos más en %@\"}";
$a["interval.day||count"] = "{\"one\":\"Cada %d día\", \"other\":\"Cada %d días\"}";
$a["streak.day||count"] = "{\"one\":\"¡Racha de %d día!\", \"other\":\"¡Racha de %d días!\"}";
$a["month||count"] = "{\"one\":\"%d mes\", \"other\":\"%d meses\"}";
$a["passed.week||count"] = "{\"one\":\"Últimas %d semanas\", \"other\":\"Últimas %d semanas\"}";
$a["passed.days||count"] = "{\"one\":\"Último día\", \"other\":\"Últimos %d días\"}";
$a["times||count"] = "{\"one\":\"%d vez\", \"other\":\"%d veces\"}";
$a["streak.day.journal||count"] = "{\"one\":\"Racha de %d día\", \"other\":\"Racha de %d días\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ de %1d hábito completado\", \"other\":\"%2#@completed@ de %1d hábitos completados\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"Racha de %d día\", \"other\":\"Racha de %d días\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"Racha de %d día\", \"other\":\"Racha de %d días\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";

return $a;
?>