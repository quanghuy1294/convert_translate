<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d dia por semana\", \"other\":\"%d dias por semana\"}";
$a["streak||count"] = "{\"one\":\"%d Sequência\", \"other\":\"%d Sequências\"}";
$a["more||count"] = "{\"one\":\"%d mais\", \"other\":\"%d mais\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"mais %u hábito de %@\", \"other\":\"mais %u hábitos de %@\"}";
$a["interval.day||count"] = "{\"one\":\"A cada %d dia\", \"other\":\"A cada %d dias\"}";
$a["streak.day||count"] = "{\"one\":\"Sequência de %d dia!\", \"other\":\"Sequência de %d dias!\"}";
$a["month||count"] = "{\"one\":\"%d mês\", \"other\":\"%d meses\"}";
$a["passed.week||count"] = "{\"one\":\"Última %d semana\", \"other\":\"Últimas %d semanas\"}";
$a["passed.days||count"] = "{\"one\":\"Último %d dia\", \"other\":\"Últimos %d dias\"}";
$a["times||count"] = "{\"one\":\"%d vez\", \"other\":\"%d vezes\"}";
$a["streak.day.journal||count"] = "{\"one\":\"Sequência de %d-dia\", \"other\":\"Sequência de %d-dias\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ de %1d hábito concluído\", \"other\":\"%2#@completed@ de %1d hábitos concluídos\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"Sequência de %d-dias\", \"other\":\"Sequência de %d-dias\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"Sequência de %d-dias\", \"other\":\"Sequência de %d-dias\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d hábitos\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"Sequência de %d-dias\", \"other\":\"Sequência de %d-dias\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d hábito\", \"other\":\"%d hábitos\"}";

return $a;
?>