<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d Tag pro Woche\", \"other\":\"%d Tage pro Woche\"}";
$a["streak||count"] = "{\"one\":\"%d Erfolgsserie\", \"other\":\"%d Erfolgsserie\"}";
$a["more||count"] = "{\"one\":\"%d mehr\", \"other\":\"%d mehr\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"Noch %u Gewohnheit am %@\", \"other\":\"Noch %u Gewohnheiten am %@\"}";
$a["interval.day||count"] = "{\"one\":\"Jeden %d Tag\", \"other\":\"Alle %d Tage\"}";
$a["streak.day||count"] = "{\"one\":\"%d Tag hintereinander!\", \"other\":\"%d Tage hintereinander!\"}";
$a["month||count"] = "{\"one\":\"%d Monat\", \"other\":\"%d Monate\"}";
$a["passed.week||count"] = "{\"one\":\"Letzte %d Woche\", \"other\":\"Letzte %d Wochen\"}";
$a["passed.days||count"] = "{\"one\":\"Letzter %d Tag\", \"other\":\"Letzte %d Tage\"}";
$a["times||count"] = "{\"one\":\"%d mal\", \"other\":\"%d mal\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d-Tagesserie\", \"other\":\"%d-Tage-Serie\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ von %1d Gewohnheit abgeschlossen\", \"other\":\"%2#@completed@ von %1d Gewohnheiten abgeschlossen\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d Tag Serie\", \"other\":\"%d Tage Serie\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d Tag Serie\", \"other\":\"%d Tage Serie\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d Gewohnheit\", \"other\":\"%d Gewohnheiten\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d Gewohnheit\", \"other\":\"%d Gewohnheiten\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d Gewohnheit\", \"other\":\"%d Gewohnheiten\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d Gewohnheit\", \"other\":\"%d Gewohnheiten\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d Gewohnheit\", \"other\":\"%d Gewohnheiten\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d Tag Serie\", \"other\":\"%d Tage Serie\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d Gewohnheit\", \"other\":\"%d Gewohnheiten\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d Gewohnheit\", \"other\":\"%d Gewohnheiten\"}";

return $a;
?>