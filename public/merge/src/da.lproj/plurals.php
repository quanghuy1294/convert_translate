<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d dag pr. uge\", \"other\":\"%d dage pr. uge\"}";
$a["streak||count"] = "{\"one\":\"%d i serie\", \"other\":\"%d i serie\"}";
$a["more||count"] = "{\"one\":\"%d mere\", \"other\":\"%d flere\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u %@-vane mere\", \"other\":\"%u %@-vaner mere\"}";
$a["interval.day||count"] = "{\"one\":\"Hver dag\", \"other\":\"Hver %d. dag\"}";
$a["streak.day||count"] = "{\"one\":\"%d-dags serie!\", \"other\":\"%d-dags serie!\"}";
$a["month||count"] = "{\"one\":\"%d måned\", \"other\":\"%d måneder\"}";
$a["passed.week||count"] = "{\"one\":\"Seneste uge\", \"other\":\"Seneste %d uger\"}";
$a["passed.days||count"] = "{\"one\":\"Seneste dag\", \"other\":\"Seneste %d dage\"}";
$a["times||count"] = "{\"one\":\"%d gang\", \"other\":\"%d gange\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d-dags serie\", \"other\":\"%d-dages serie\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ af %1d vane fuldført\", \"other\":\"%2#@completed@ af %1d vaner fuldført\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d-dags serie\", \"other\":\"%d-dages serie\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d-dags serie\", \"other\":\"%d-dages serie\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d vane\", \"other\":\"%d vaner\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d vane\", \"other\":\"%d vaner\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d vane\", \"other\":\"%d vaner\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d vane\", \"other\":\"%d vaner\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d vane\", \"other\":\"%d vaner\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d-dags serie\", \"other\":\"%d-dages serie\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d vane\", \"other\":\"%d vaner\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d vane\", \"other\":\"%d vaner\"}";

return $a;
?>