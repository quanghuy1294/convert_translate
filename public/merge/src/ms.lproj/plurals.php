<?php
$a = [];
$a["day.per.week||count"] = "{\"other\":\"%d days per week\"}";
$a["streak||count"] = "{\"other\":\"%d Streaks\"}";
$a["more||count"] = "{\"other\":\"%d more\"}";
$a["time.of.day.notification.group||notifications"] = "{\"other\":\"%u more habits in %@\"}";
$a["interval.day||count"] = "{\"other\":\"Every %d days\"}";
$a["streak.day||count"] = "{\"other\":\"%d days streaks!\"}";
$a["month||count"] = "{\"other\":\"%d months\"}";
$a["passed.week||count"] = "{\"other\":\"Last %d weeks\"}";
$a["passed.days||count"] = "{\"other\":\"Last %d days\"}";
$a["times||count"] = "{\"other\":\"%d times\"}";
$a["streak.day.journal||count"] = "{\"other\":\"%d-day streak\"}";
$a["habit.remaining||required"] = "{\"other\":\"%2#@completed@ of %1d habits completed\"}";
$a["habit.remaining||completed"] = "{\"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"other\":\"%d-day streak\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"other\":\"%d-day streak\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"other\":\"%d habits\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"other\":\"%d habits\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"other\":\"%d-day streak\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"other\":\"%d habits\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"other\":\"%d habits\"}";

return $a;
?>