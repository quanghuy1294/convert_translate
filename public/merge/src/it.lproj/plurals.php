<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d giorno alla settimana\", \"other\":\"%d giorni alla settimana\"}";
$a["streak||count"] = "{\"one\":\"%d Serie\", \"other\":\"%d Serie\"}";
$a["more||count"] = "{\"one\":\"%d di più\", \"other\":\"%d di più\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u un' altra abitudine in %@\", \"other\":\"%u altre abitudini in %@\"}";
$a["interval.day||count"] = "{\"one\":\"Ogni %d giorno\", \"other\":\"Ogni %d giorni\"}";
$a["streak.day||count"] = "{\"one\":\"Serie di %d giorno!\", \"other\":\"Serie di %d giorni!\"}";
$a["month||count"] = "{\"one\":\"%d mese\", \"other\":\"%d mesi\"}";
$a["passed.week||count"] = "{\"one\":\"Ultime %d settimane\", \"other\":\"Ultime %d settimane\"}";
$a["passed.days||count"] = "{\"one\":\"Ultimi %d giorni\", \"other\":\"Ultimi %d giorni\"}";
$a["times||count"] = "{\"one\":\"%d volta\", \"other\":\"%d volte\"}";
$a["streak.day.journal||count"] = "{\"one\":\"serie %d-giorni\", \"other\":\"serie %d-giorni\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ of %1d abitudine completata\", \"other\":\"%2#@completed@ of %1d abitudini completate\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d giorno\", \"other\":\"%d giorni di fila\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"serie %d-giorni\", \"other\":\"serie %d-giorni\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d abitudine\", \"other\":\"%d abitudini\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d abitudine\", \"other\":\"%d abitudini\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d abitudine\", \"other\":\"%d abitudini\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d abitudine\", \"other\":\"%d abitudini\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d abitudine\", \"other\":\"%d abitudini\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"serie %d-giorni\", \"other\":\"serie %d-giorni\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d abitudine\", \"other\":\"%d abitudini\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d abitudine\", \"other\":\"%d abitudini\"}";

return $a;
?>