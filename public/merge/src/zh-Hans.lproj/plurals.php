<?php
$a = [];
$a["day.per.week||count"] = "{\"other\":\"每周 %d 天\"}";
$a["streak||count"] = "{\"other\":\"连续完成天数：%d\"}";
$a["more||count"] = "{\"other\":\"再来%d个\"}";
$a["time.of.day.notification.group||notifications"] = "{\"other\":\"%@还有 %u 个习惯\"}";
$a["interval.day||count"] = "{\"other\":\"每 %d 天\"}";
$a["streak.day||count"] = "{\"other\":\"连续完成 %d 天！\"}";
$a["month||count"] = "{\"other\":\"%d 个月\"}";
$a["passed.week||count"] = "{\"other\":\"过去 %d 周\"}";
$a["passed.days||count"] = "{\"other\":\"过去 %d 天\"}";
$a["times||count"] = "{\"other\":\"%d 次\"}";
$a["streak.day.journal||count"] = "{\"other\":\"%d 天连续完成 ！\"}";
$a["habit.remaining||required"] = "{\"other\":\"%1d个习惯中已完成%2#@completed@ 个\"}";
$a["habit.remaining||completed"] = "{\"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"other\":\"%d 天连续完成 ！\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"other\":\"%d 天连续完成 ！\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"other\":\"%d习惯\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"other\":\"%d习惯\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"other\":\"%d习惯\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"other\":\"%d个习惯\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"other\":\"%d习惯\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"other\":\"%d 天连续完成 ！\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"other\":\"%d习惯\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"other\":\"%d习惯\"}";

return $a;
?>