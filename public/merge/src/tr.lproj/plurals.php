<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"Haftada %d gün\", \"other\":\"Haftada %d gün\"}";
$a["streak||count"] = "{\"one\":\"%d seri\", \"other\":\"%d seri\"}";
$a["more||count"] = "{\"one\":\"%d daha fazlası\", \"other\":\"%d daha fazlası\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"Bu %@ için %u tane daha alışkanlık\", \"other\":\"Bu %@ için %u tane daha alışkanlık\"}";
$a["interval.day||count"] = "{\"one\":\"Her %d günde bir\", \"other\":\"Her %d günde bir\"}";
$a["streak.day||count"] = "{\"one\":\"%d günlük seri!\", \"other\":\"%d günlük seri!\"}";
$a["month||count"] = "{\"one\":\"%d ay\", \"other\":\"%d ay\"}";
$a["passed.week||count"] = "{\"one\":\"Son %d hafta\", \"other\":\"Son %d hafta\"}";
$a["passed.days||count"] = "{\"one\":\"Son %d gün\", \"other\":\"Son %d gün\"}";
$a["times||count"] = "{\"one\":\"%d defa\", \"other\":\"%d defa\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d gün seri\", \"other\":\"%d gün seri\"}";
$a["habit.remaining||required"] = "{\"one\":\"%1d alışkanlığın %2#@completed@ tanesi tamamlandı\", \"other\":\"%1d alışkanlığın %2#@completed@ tanesi tamamlandı\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d-gün seri\", \"other\":\"%d-gün seri\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d-gün seri\", \"other\":\"%d-gün seri\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d alışkanlık\", \"other\":\"%d alışkanlık\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d alışkanlık\", \"other\":\"%d alışkanlık\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d alışkanlık\", \"other\":\"%d alışkanlık\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d alışkanlık\", \"other\":\"%d alışkanlık\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d alışkanlık\", \"other\":\"%d alışkanlık\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d-gün seri\", \"other\":\"%d-gün seri\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d alışkanlık\", \"other\":\"%d alışkanlık\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d alışkanlık\", \"other\":\"%d alışkanlık\"}";

return $a;
?>