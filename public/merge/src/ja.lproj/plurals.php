<?php
$a = [];
$a["day.per.week||count"] = "{\"other\":\"週に%d日\"}";
$a["streak||count"] = "{\"other\":\"%d連続\"}";
$a["more||count"] = "{\"other\":\"その他 %d 件\"}";
$a["time.of.day.notification.group||notifications"] = "{\"other\":\"%@はほかに%u個の習慣があります\"}";
$a["interval.day||count"] = "{\"other\":\"%d日ごと\"}";
$a["streak.day||count"] = "{\"other\":\"%d日連続で達成！\"}";
$a["month||count"] = "{\"other\":\"%dか月\"}";
$a["passed.week||count"] = "{\"other\":\"過去%d週間\"}";
$a["passed.days||count"] = "{\"other\":\"過去%d日\"}";
$a["times||count"] = "{\"other\":\"%d回\"}";
$a["streak.day.journal||count"] = "{\"other\":\"%d日連続\"}";
$a["habit.remaining||required"] = "{\"other\":\"%2#@completed@ / %1d の習慣を達成\"}";
$a["habit.remaining||completed"] = "{\"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"other\":\"%d日連続\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"other\":\"%d日連続\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"other\":\"%d個の習慣\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"other\":\"%d個の習慣\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"other\":\"%d個の習慣\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"other\":\"%d個\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"other\":\"%d個の習慣\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"other\":\"%d日連続\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"other\":\"%d個の習慣\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"other\":\"%d個の習慣\"}";

return $a;
?>