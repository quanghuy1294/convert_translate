<?php
$a = [];
$a["day.per.week||count"] = "{\"zero\":\"%d day per week\", \"one\":\"%d day per week\", \"few\":\"%d days per week\", \"many\":\"%d days per week\", \"other\":\"%d days per week\"}";
$a["streak||count"] = "{\"zero\":\"%d Streak\", \"one\":\"%d Streak\", \"few\":\"%d Streaks\", \"many\":\"%d Streaks\", \"other\":\"%d Streaks\"}";
$a["more||count"] = "{\"zero\":\"%d more\", \"one\":\"%d more\", \"few\":\"%d more\", \"many\":\"%d more\", \"other\":\"%d more\"}";
$a["time.of.day.notification.group||notifications"] = "{\"zero\":\"%u more habit in %@\", \"one\":\"%u more habit in %@\", \"few\":\"%u more habits in %@\", \"many\":\"%u more habits in %@\", \"other\":\"%u more habits in %@\"}";
$a["interval.day||count"] = "{\"zero\":\"Every %d day\", \"one\":\"Every %d day\", \"few\":\"Every %d days\", \"many\":\"Every %d days\", \"other\":\"Every %d days\"}";
$a["streak.day||count"] = "{\"zero\":\"%d day streaks!\", \"one\":\"%d day streaks!\", \"few\":\"%d days streaks!\", \"many\":\"%d days streaks!\", \"other\":\"%d days streaks!\"}";
$a["month||count"] = "{\"zero\":\"%d month\", \"one\":\"%d month\", \"few\":\"%d months\", \"many\":\"%d months\", \"other\":\"%d months\"}";
$a["passed.week||count"] = "{\"zero\":\"Last %d week\", \"one\":\"Last %d week\", \"few\":\"Last %d weeks\", \"many\":\"Last %d weeks\", \"other\":\"Last %d weeks\"}";
$a["passed.days||count"] = "{\"zero\":\"Last %d day\", \"one\":\"Last %d day\", \"few\":\"Last %d days\", \"many\":\"Last %d days\", \"other\":\"Last %d days\"}";
$a["times||count"] = "{\"zero\":\"%d time\", \"one\":\"%d time\", \"few\":\"%d times\", \"many\":\"%d times\", \"other\":\"%d times\"}";
$a["streak.day.journal||count"] = "{\"zero\":\"%d-day streak\", \"one\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ of %1d habit completed\", \"few\":\"%2#@completed@ of %1d habits completed\", \"many\":\"%2#@completed@ of %1d habits completed\", \"other\":\"%2#@completed@ of %1d habits completed\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"few\":\"%d\", \"many\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"zero\":\"%d habit\", \"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"zero\":\"%d habit\", \"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"zero\":\"%d habit\", \"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";

return $a;
?>