<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d день на тиждень\", \"few\":\"%d днів на тиждень\", \"many\":\"%d днів на тиждень\", \"other\":\"%d дні на тиждень\"}";
$a["streak||count"] = "{\"one\":\"%d серія\", \"few\":\"%d серій\", \"many\":\"%d серій\", \"other\":\"%d серій\"}";
$a["more||count"] = "{\"one\":\"Ще %d\", \"few\":\"Ще %d\", \"many\":\"Ще %d\", \"other\":\"Ще %d\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u more habit in %@\", \"few\":\"%u more habits in %@\", \"many\":\"%u more habits in %@\", \"other\":\"%u more habits in %@\"}";
$a["interval.day||count"] = "{\"one\":\"Every %d day\", \"few\":\"Every %d days\", \"many\":\"Every %d days\", \"other\":\"Every %d days\"}";
$a["streak.day||count"] = "{\"one\":\"%d day streaks!\", \"few\":\"%d days streaks!\", \"many\":\"%d days streaks!\", \"other\":\"%d days streaks!\"}";
$a["month||count"] = "{\"one\":\"%d month\", \"few\":\"%d months\", \"many\":\"%d months\", \"other\":\"%d months\"}";
$a["passed.week||count"] = "{\"one\":\"Last %d week\", \"few\":\"Last %d weeks\", \"many\":\"Last %d weeks\", \"other\":\"Last %d weeks\"}";
$a["passed.days||count"] = "{\"one\":\"Last %d day\", \"few\":\"Last %d days\", \"many\":\"Last %d days\", \"other\":\"Last %d days\"}";
$a["times||count"] = "{\"one\":\"%d time\", \"few\":\"%d times\", \"many\":\"%d times\", \"other\":\"%d times\"}";
$a["streak.day.journal||count"] = "{\"one\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ of %1d habit completed\", \"few\":\"%2#@completed@ of %1d habits completed\", \"many\":\"%2#@completed@ of %1d habits completed\", \"other\":\"%2#@completed@ of %1d habits completed\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"few\":\"%d\", \"many\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d звички\", \"few\":\"%d звички\", \"many\":\"%d звичок\", \"other\":\"%d звичок\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"%d-day streak\", \"few\":\"%d-day streak\", \"many\":\"%d-day streak\", \"other\":\"%d-day streak\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d habit\", \"few\":\"%d habits\", \"many\":\"%d habits\", \"other\":\"%d habits\"}";

return $a;
?>