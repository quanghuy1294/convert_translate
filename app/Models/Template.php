<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $fillable = ['name', 'lang'];

    public function detail() {
        return $this->hasMany(TemplateDetail::class, 'template_id');
    }
}
