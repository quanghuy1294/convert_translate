<?php
$a = [];
$a["day.per.week||count"] = "{\"one\":\"%d дня в неделю\", \"few\":\"%d дня в неделю\", \"many\":\"%d дней в неделю\", \"other\":\"%d дн. в неделю\"}";
$a["streak||count"] = "{\"one\":\"%d День Подряд\", \"few\":\"%d Дня Подряд\", \"many\":\"%d Дней Подряд\", \"other\":\"%d Дн. Подряд\"}";
$a["more||count"] = "{\"one\":\"Еще %d\", \"few\":\"Еще %d\", \"many\":\"Еще %d\", \"other\":\"Еще %d\"}";
$a["time.of.day.notification.group||notifications"] = "{\"one\":\"%u more habit in %@\", \"few\":\"%u more habits in %@\", \"many\":\"%u more habits in %@\", \"other\":\"%u more habits in %@\"}";
$a["interval.day||count"] = "{\"one\":\"Каждый %d день\", \"few\":\"Каждые %d дня\", \"many\":\"Каждые %d дней\", \"other\":\"Каждые %d дн.\"}";
$a["streak.day||count"] = "{\"one\":\"%d день подряд!\", \"few\":\"%d дня подряд!\", \"many\":\"%d дней подряд!\", \"other\":\"%d дн. подряд!\"}";
$a["month||count"] = "{\"one\":\"%d месяц\", \"few\":\"%d месяца\", \"many\":\"%d месяцев\", \"other\":\"%d мес.\"}";
$a["passed.week||count"] = "{\"one\":\"Последняя %d неделя\", \"few\":\"Последние %d недели\", \"many\":\"Последняя %d недель\", \"other\":\"Последние %d нед.\"}";
$a["passed.days||count"] = "{\"one\":\"Вчера\", \"few\":\"Последние %d дня\", \"many\":\"Последние %d дней\", \"other\":\"Последние %d дн.\"}";
$a["times||count"] = "{\"one\":\"%d раз\", \"few\":\"%d раза\", \"many\":\"%d раз\", \"other\":\"%d раз\"}";
$a["streak.day.journal||count"] = "{\"one\":\"Комбо %d день\", \"few\":\"Комбо %d дня\", \"many\":\"Комбо %d дней\", \"other\":\"Комбо %d дней\"}";
$a["habit.remaining||required"] = "{\"one\":\"%2#@completed@ из %1d привычки завершено\", \"few\":\"%2#@completed@ из %1d привычек завершено\", \"many\":\"%2#@completed@ из %1d привычек завершено\", \"other\":\"%2#@completed@ из %1d привычек завершено\"}";
$a["habit.remaining||completed"] = "{\"one\":\"%d\", \"few\":\"%d\", \"many\":\"%d\", \"other\":\"%d\"}";
$a["habit.notification.on.streak.less.than.ten||streak"] = "{\"one\":\"Комбо %d день\", \"few\":\"Комбо %d дня\", \"many\":\"Комбо %d дней\", \"other\":\"Комбо %d дней\"}";
$a["habit.notification.on.streak.more.than.ten||streak"] = "{\"one\":\"Комбо %d день\", \"few\":\"Комбо %d дня\", \"many\":\"Комбо %d дней\", \"other\":\"Комбо %d дней\"}";
$a["daily.briefing.evening.inprogress||completed"] = "{\"one\":\"%d привычка\", \"few\":\"%d привычки\", \"many\":\"%d привычек\", \"other\":\"%d привычек\"}";
$a["daily.briefing.evening.inprogress||skipped"] = "{\"one\":\"%d привычка\", \"few\":\"%d привычек\", \"many\":\"%d привычек\", \"other\":\"%d привычек\"}";
$a["daily.briefing.evening.inprogress||failed"] = "{\"one\":\"%d привычка\", \"few\":\"%d привычек\", \"many\":\"%d привычек\", \"other\":\"%d привычек\"}";
$a["daily.briefing.evening.inprogress2||required"] = "{\"one\":\"%d привычка\", \"few\":\"%d привычки\", \"many\":\"%d привычек\", \"other\":\"%d привычек\"}";
$a["daily.briefing.evening.weekly.multiple.habit.completed||habit_count"] = "{\"one\":\"%d привычка\", \"few\":\"%d привычки\", \"many\":\"%d привычек\", \"other\":\"%d привычек\"}";
$a["daily.briefing.evening.favourite.habit||%2$#@streak_day_count@"] = "{\"one\":\"Комбо %d день\", \"few\":\"Комбо %d дня\", \"many\":\"Комбо %d дней\", \"other\":\"Комбо %d дней\"}";
$a["daily.briefing.morning.multiple||pending"] = "{\"one\":\"%d привычка\", \"few\":\"%d привычек\", \"many\":\"%d привычек\", \"other\":\"%d привычек\"}";
$a["daily.briefing.morning.multiple.no.reminder||pending"] = "{\"one\":\"%d привычка\", \"few\":\"%d привычки\", \"many\":\"%d привычек\", \"other\":\"%d привычек\"}";

return $a;
?>