<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemplateDetail extends Model
{
    protected $table = 'template_detail';
    protected $fillable = ['template_id', 'key', 'value'];
}
