<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="{{ route('import.mergeProcess') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="">Language: </label>
            <select name="lang" id="">
                @foreach (list_lang() as $id => $lang)
                    <option value="{{ $id }}">{{ $lang }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="">File</label>
            <input type="file" name="file[]" multiple="">
        </div>
        <button class="btn-btn-primary">Submit</button>
    </form>
</body>
</html>